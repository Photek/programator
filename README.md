3 zadania:

ZADANIE 1:
Napisz program, który posłuży Ci jako menadżer haseł. Program powinien mieć następujące funkcjonalności:

- Po podaniu adresu strony oraz nazwy użytkownika program zwraca zapisane dla danego loginu hasło
- Jeśli dla takiej domeny/loginu program nie posiada hasła, użytkownik otrzymuje odpowiedni komunikat
- Program potrafi na żądanie wygenerować nowe hasło (losowy ciąg znaków alfanumerycznych, można zaimplementować samemu lub użyć jakiegoś gotowego rozwiązania z sieci)
- Użytkownik ma możliwość dodania nowego hasła dla wybranej pary domena/login
    - Program nie przyjmuje duplikatów. Jeśli wpis istnieje, powinien poprosić użytkownika o to czy chce nadpisać hasło.
- Program potrafi wyświetlić pełny raport, posortowany po adresach domen, a następnie po loginach. Raport wyświetla Adres domeny, Login oraz zapamiętane hasło
- Wszystkie dane trzymamy w pamięci w odpowiednich strukturach danych (tj. bez bazy danych)

ZADANIE 2:
Napisz program do obsługi biblioteki. Ma posiadać następujące funkcjonalności:

- Program działa dopóki użytkownik nie wpisze "Exit", każda funkcjonalność oferuje podmenu i 
    możliwość przemieszczania się między funkcjonalnościami
- Katalog książek (wystarczy kilka) do wypożyczenia (wszystkie możliwe książki powinny być zapisane w pliku)
    - Książka musi mieć minimum: tytuł, rok wydania, autora oraz swoje ID (proponuję użyć tutaj ISBN)
- Dane o wypożyczonych książkach trzymamy w pamięci (wybierz odpowiednią strukturę danych), stwórz klasę, która będzie reprezentować repozytorium książek
    - Ta klasa jako jedyna ma możliwość wykonywania operacji typu weź książkę, wypożycz książkę itp.
- Przez interakcję przez konsolę, użytkownik może wypożyczyć egzemplarz danej książki, jeśli jest on dostępny. Jeśli nie, powinien się o tym dowiedzieć 
- Użytkownik może wyświetlić listę wszystkich dostępnych książek
- Użytkownik może wyszukać książki po wszystkich atrybutach (fraza tytułu, rok wydania, autor, ID)
- Użytkownik może dodać nową książkę do zbioru (donacja)
    - Biblioteka nie przyjmuje duplikatów, jeśli książka o zadanym ISBN już istnieje w zbiorze, nie przyjmuje donacji
- Spróbuj zrobić kod obiektowo, tj. przy użyciu jak najmniejszej ilości statycznych metod i pól. 
    Wyjątkiem jest metoda main lub jeśli używasz słówka static świadomie (np. deklarując jakąś stałą w klasie).
    
ZADANIE 3:
Napisz program, który pomoże w obsłudze wypożyczalni pojazdów. Oto wymagania:

- Możliwość rejestracji i logowania się przez użytkownika
    - Dane przechowujemy w pamięci, wystarczy login i hasło
    - Niezalogowany użytkownik nie może korzystać z żadnych innych funkcjonalności niż logowanie / rejestracja
- Baza danych zawierająca dostępne pojazdy (w pamięci)
    - Wystarczą jakieś stałe dane, nie trzeba implementować dodawania nowych (dla chętnych: można, ale tylko przez użytkownika typu admin)
    - Źródło danych oraz sama baza niech będą oddzielnymi klasami
- Pojazdy posiadają kilka atrybutów
    - ID
    - Marka, model, rocznik, liczba KM, typ silnika, pojemność silnika, nr rejestracyjny
    - Typ pojazdu (do tego celu zastosuj dziedziczenie)
    - Wymagania dotyczące prowadzenia pojazdu (np. kategoria prawa jazdy, inna dla motorów, samochodów i ciężarówek) -> to również zaimplementuj przy użyciu dziedziczenia
        - Spróbuj wymyślić własne wymagania (np. wiekowe, umiejętności itp.)
    - Cena za godzinę oraz cena za dzień
- Możliwość wyceny wynajmu danego pojazdu w określonym okresie (pełne dni liczone po stawce dziennej, reszta po stawce godzinowej)
- Możliwość wypożyczenia pojazdu, jeśli jest on dostępny (nikt inny go nie wypożyczył w danym terminie)
    - Zwróć uwagę, że w tym celu musisz mieć min. 2 użytkowników
    - Zwróć uwagę, że określanie dostępności, wypożyczanie (tj. rezerwowanie dostępności) to inna odpowiedzialność niż informowanie o dostępnych pojazdach
- Możliwość wyświetlenia listy wszystkich pojazdów wraz z informacją o tym, w których terminach pojazdy są już zarezerwowane


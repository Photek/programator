package com.programator.zadanie01_passvault;

import java.util.Objects;

public class DomainAndLogin {
    private String domain;
    private String login;

    public DomainAndLogin(String domain, String login) {
        this.domain = domain;
        this.login = login;
    }

    public String getDomain() {
        return domain;
    }

    public String getLogin() {
        return login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DomainAndLogin that = (DomainAndLogin) o;

        if (!Objects.equals(domain, that.domain)) return false;
        return Objects.equals(login, that.login);
    }

    @Override
    public int hashCode() {
        int result = domain != null ? domain.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        return result;
    }
}

package com.programator.zadanie01_passvault;

import java.util.Comparator;

public class DomainLoginComparator implements Comparator<DomainAndLogin> {


    @Override
    public int compare(DomainAndLogin o1, DomainAndLogin o2) {
        if (o1.getDomain().equals(o2.getDomain())){
            return o1.getLogin().compareTo(o2.getLogin());
        }
        return (o1.getDomain().compareTo(o2.getDomain()));
    }
}

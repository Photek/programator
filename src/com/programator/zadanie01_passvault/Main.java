package com.programator.zadanie01_passvault;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        Menu menu = new Menu();
        PassDB passDB = new PassDB();
        passDB.addPassword(new DomainAndLogin("wp.pl", "u3"), "p1");
        passDB.addPassword(new DomainAndLogin("wp.pl", "u1"), "p1");
        passDB.addPassword(new DomainAndLogin("wp.pl", "u2"), "p1");
        passDB.addPassword(new DomainAndLogin("onet.pl", "u2"), "p1");
        passDB.addPassword(new DomainAndLogin("google.pl", "u1"), "p1");
        passDB.addPassword(new DomainAndLogin("dropbox.com", "u4"), "p1");
        passDB.addPassword(new DomainAndLogin("slack.com", "u1"), "p1");
        String domain;
        String login;
        String password;

        menu.showMenu();

        while (true) {
            String input = readInput();
            switch (input) {
                case "1":
                    System.out.print("Wpisz nazwę domeny: ");
                    domain = readInput();
                    System.out.print("Wpisz login: ");
                    login = readInput();
                    System.out.println(passDB.showPassword(new DomainAndLogin(domain, login)));
                    menu.showMenu();
                    break;
                case "2":
                    System.out.print("Wpisz nazwę domeny: ");
                    domain = readInput();
                    System.out.print("Wpisz login: ");
                    login = readInput();
                    DomainAndLogin domainAndLogin = new DomainAndLogin(domain, login);

                    if (passDB.isPasswordInDB(domainAndLogin)) {
                        System.out.println("Podana domena i użytkownik istnieją już w bazie.");
                        upup:
                        while (true) {
                            System.out.println("Czy zmienić hasło? (T/N):");
                            String yesNo = readInput();

                            if (yesNo.toLowerCase().equals("t")) {
                                System.out.println("Wpisz nowe hasło (W) / Generuj nowe hasło (G): ");

                                while (true) {
                                    String typeOrGenerate = readInput();

                                    if (typeOrGenerate.toLowerCase().equals("w")) {
                                        System.out.print("Wpisz nowe hasło: ");
                                        password = readInput();
                                        passDB.addPassword(domainAndLogin, password);
                                        System.out.println("Hasło zostało zmienione.");
                                        break upup;
                                    }

                                    if (typeOrGenerate.toLowerCase().equals("g")) {
                                        System.out.println("Podaj ile znaków ma mieć wygenerowane hasło: ");

                                        while (true) {
                                            String nString = readInput();
                                            try {
                                                int n = Integer.parseInt(nString);
                                                password = passDB.generatePassword(n);
                                                passDB.addPassword(domainAndLogin, password);
                                                System.out.println("Hasło zostało zmienione na wygenerowane.");
                                                break upup;
                                            } catch (NumberFormatException e) {
                                                System.out.println("Wprowadź liczbę!");
                                            }
                                        }
                                    } else {
                                        System.out.println("Wpisz 'W' lub 'G' !");
                                    }
                                }
                            }
                            if (yesNo.toLowerCase().equals("n")) {
                                System.out.println("Hasło nie zostało zmienione.");
                                break;
                            }
                        }

                    } else {
                        System.out.print("Wpisz hasło: ");
                        password = readInput();
                        passDB.addPassword(domainAndLogin, password);
                        System.out.println("Hasło zostało dodane.");
                    }
                    menu.showMenu();
                    break;
                case "3":
                    System.out.println("Zapisane hasła:");
                    passDB.showAllPasswords();
                    System.out.println();
                    menu.showMenu();
                    break;
                case "4":
                    System.out.println("Zapisane hasła (posortowane):");
                    passDB.showAllPasswordsSorted();
                    System.out.println();
                    menu.showMenu();
                case "5":
                    System.out.println("Bye, bye...");
                    return;
                default:
                    System.out.print("Wprowadź liczbę od 1-5: ");
            }
        }
    }

    public static String readInput() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }

}

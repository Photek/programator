package com.programator.zadanie01_passvault;

public class Menu {

    public void showMenu() {
        System.out.println("1. Pokaż hasło.");
        System.out.println("2. Wprowadź/generuj nowe hasło.");
        System.out.println("3. Wyświetl zapisane hasła.");
        System.out.println("4. Wyświetl zapisane hasła (posortowane) .");
        System.out.println("5. Wyjdź.");
        System.out.println("Wybierz polecenie (1-5): ");
    }

}
package com.programator.zadanie01_passvault;

import java.util.*;

public class PassDB {
    private Map<DomainAndLogin, String> passDB = new HashMap<>();

    public String showPassword(DomainAndLogin domainAndLogin) {
        if (passDB.containsKey(domainAndLogin)) {
            return "Hasło dla podanej domeny i loginu: " + passDB.get(domainAndLogin);
        }
        return "Nie ma takiej domeny i loginu w bazie :P";
    }

    public boolean isPasswordInDB(DomainAndLogin domainAndLogin) {
        if (passDB.containsKey(domainAndLogin)) {
            return true;
        }
        return false;
    }

    public void addPassword(DomainAndLogin domainAndLogin, String password) {
        passDB.put(domainAndLogin, password);
    }

    public String generatePassword(int n) {
        Random random = new Random();
        StringBuilder password = new StringBuilder();
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*+=";
        for (int i = 0; i < n; i++) {
            password.append(chars.charAt(random.nextInt(chars.length())));
        }
        return password.toString();
    }

    public void showAllPasswords() {
        for (DomainAndLogin key : passDB.keySet()) {
            System.out.println(key.getDomain() + " || " + key.getLogin() + " || " + passDB.get(key));
        }
    }

    public void showAllPasswordsSorted(){
        List<DomainAndLogin> domainAndLoginList = new ArrayList<>();
        for (DomainAndLogin key : passDB.keySet()) {
            domainAndLoginList.add(key);
        }
        Collections.sort(domainAndLoginList,new DomainLoginComparator());
        for (DomainAndLogin key : domainAndLoginList) {
            System.out.println(key.getDomain() + " || " + key.getLogin() + " || " + passDB.get(key));
        }
    }


}

package com.programator.zadanie02_booklibrary;

public class Book {
    private String title;
    private String authorName;
    private String authorSurname;
    private int year;
    private String ISBN;

    public Book(String title, String authorName, String authorSurname, int year, String ISBN) {
        this.title = title;
        this.authorName = authorName;
        this.authorSurname = authorSurname;
        this.year = year;
        this.ISBN = ISBN;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public String getISBN() {
        return ISBN;
    }

    @Override
    public String toString() {
        return title + " || " + authorName + " " + authorSurname + " || "
                + year + " || " + ISBN;
    }
}

package com.programator.zadanie02_booklibrary;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

public class BookLibrary {
    public static void main(String[] args) throws IOException {
        MainMenu mainMenu = new MainMenu();
        BookLibraryDB bookLibraryDB = new BookLibraryDB();
        BookLibraryStateDB bookLibraryStateDB = new BookLibraryStateDB();
        bookLibraryDB.loadDBFromFile();
        bookLibraryStateDB.loadDBFromFile();

        String x = mainMenu.showMainMenu();

        while (!(x.equals("exit"))) {
            switch (x) {
                case "1":       // wyswietl wszystkie ksiazki
                    for (Book book : bookLibraryDB.getAllBooks()) {
                        System.out.println(book + " | dostępnych: | " + bookLibraryStateDB.getAvailableQuantity(book.getISBN()));
                    }
                    x = mainMenu.showSubMenu1();
                    break;

                case "11":      // wyswietl dostepne ksiazki
                    Set<String> foundedISBNs = bookLibraryStateDB.getISBNsOfAllAvailableBooks();
                    for (String foundedISBN : foundedISBNs) {
                        System.out.println(bookLibraryDB.getByISBN(foundedISBN) + " | dostępnych: | " + bookLibraryStateDB.getAvailableQuantity(foundedISBN));
                    }
                    System.out.println();
                    x = mainMenu.showSubMenu11();
                    break;

                case "12":      // wypozycz ksiazke
//                    String isbn = bookLibraryDB.getISBNFromTitle(mainMenu.showSubMenu12());
                    String isbn = mainMenu.showSubMenu24();
                    Optional<Book> bookOp = bookLibraryDB.getByISBN(isbn);

                    if (!bookOp.isPresent()) {
                        System.out.println("Nie ma w bibliotece książki o podanym nr ISBN.");
                    } else {
                        if (bookLibraryStateDB.isBookAvailable(isbn)) {
//                            Book book = bookLibraryDB.getByISBN(isbn);
                            System.out.println("Wypożyczyłeś książkę - Tytuł: " + bookOp.get().getTitle() + " || Autor: " + bookOp.get().getAuthorName() + " " + bookOp.get().getAuthorSurname());
                            bookLibraryStateDB.setLendQuantity(isbn);
                            bookLibraryDB.saveDBToFile(bookLibraryStateDB);
                        } else {
                            System.out.println("Chwilowo brak dostępnych egzemplarzy książki.");
                        }
                    }
                    System.out.println();
                    x = mainMenu.showMainMenu();
                    break;

                case "2":       // wyszukaj ksiazke:
                    x = mainMenu.showSubMenu2();
                    break;

                case "21":      // wyszukaj po tytule
                    Set<Book> foundedBooks = bookLibraryDB.getByTitle(mainMenu.showSubMenu21());
                    if (foundedBooks.isEmpty()) {
                        System.out.println("Nie ma w bibliotece książki o podanym tytule.");
                    } else {
                        System.out.println("Znalazłem następujące ksiązki:");
                        for (Book book : foundedBooks) {
                            System.out.println(book);
                        }
                        System.out.println("Znaleziono: " + foundedBooks.size());
                    }
                    System.out.println();
                    x = mainMenu.showMainMenu();
                    break;

                case "22":      // wyszukaj po nazwisku autora
                    foundedBooks = bookLibraryDB.getByAuthorSurname(mainMenu.showSubMenu22());
                    if (foundedBooks.isEmpty()) {
                        System.out.println("Nie ma w bibliotece książek podanego autora.");
                    } else {
                        System.out.println("Znalazłem następujące ksiązki:");
                        for (Book book : foundedBooks) {
                            System.out.println(book);
                        }
                        System.out.println("Znaleziono: " + foundedBooks.size());
                    }
                    System.out.println();
                    x = mainMenu.showMainMenu();
                    break;

                case "23":      // wyszukaj po dacie wydania
                    foundedBooks = bookLibraryDB.getByYear(mainMenu.showSubMenu23());
                    if (foundedBooks.isEmpty()) {
                        System.out.println("Nie ma w bibliotece książek wydanych w podanym roku.");
                    } else {
                        System.out.println("Znalazłem następujące ksiązki:");
                        for (Book book : foundedBooks) {
                            System.out.println(book);
                        }
                        System.out.println("Znaleziono: " + foundedBooks.size());
                    }
                    System.out.println();
                    x = mainMenu.showMainMenu();
                    break;

                case "24":      // wyszukaj po ISBN
                    Optional<Book> foundedBookOp = bookLibraryDB.getByISBN(mainMenu.showSubMenu24());
                    if (!foundedBookOp.isPresent()) {
                        System.out.println("Nie ma w bibliotece książki o podanym ISBN.");
                    } else {
                        System.out.println("Znalazłem następującą książkę:");
                        Book foundedBook = foundedBookOp.get();
                        System.out.println(foundedBook);
                    }
                    System.out.println();
                    x = mainMenu.showMainMenu();
                    break;

                case "3":       // podaruj ksiazke
                    String no;
                    no = mainMenu.showSubMenu3();
                    if (bookLibraryDB.isBookInLibrary(no)) {
                        System.out.println("Dziękujemy, ale mamy już podaną książkę w bibliotece.");
                        System.out.println();
                    } else {
                        System.out.println("Dziękujemy za hojność :) Nie mamy jeszcze takiej książki w bibliotece.");
                        System.out.println("Podaj więcej informacji, aby dodać książkę do biblioteki");
                        Book book = new Book(mainMenu.title(), mainMenu.name(), mainMenu.surname(), mainMenu.year(), no);
                        bookLibraryDB.addBook(book);
                        bookLibraryStateDB.addBook(book.getISBN());
                        System.out.println("Dodaliśmy książkę do biblioteki!");
                        bookLibraryDB.saveDBToFile(bookLibraryStateDB);
                    }
                    System.out.println();
                    x = mainMenu.showMainMenu();
                    break;

                case "mm":      // wyjscie do MainMenu
                    System.out.println();
                    x = mainMenu.showMainMenu();
                    break;
            }
        }
    }
}

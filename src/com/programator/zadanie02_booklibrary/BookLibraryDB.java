package com.programator.zadanie02_booklibrary;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class BookLibraryDB {
    private Map<String, Book> library = new HashMap<>();

    public void addBook(Book book) {
        library.put(book.getISBN(), book);
    }

    public boolean isBookInLibrary(String isbn) {
        return library.containsKey(isbn);
    }

    public Collection<Book> getAllBooks() {
        return library.values();
    }

    public Set<Book> getByTitle(String title) {
        Set<Book> foundedBooks = new HashSet<>();
        for (String key : library.keySet()) {
            if (library.get(key).getTitle().toLowerCase().contains(title.toLowerCase())) {
                foundedBooks.add(library.get(key));
            }
        }
        return foundedBooks;
    }

    public Set<Book> getByAuthorSurname(String surname) {
        Set<Book> foundedBooks = new HashSet<>();
        for (String key : library.keySet()) {
            if (library.get(key).getAuthorSurname().equalsIgnoreCase(surname)) {
                foundedBooks.add(library.get(key));
            }
        }
        return foundedBooks;
    }

    public Set<Book> getByYear(int year) {
        Set<Book> foundedBooks = new HashSet<>();
        for (String key : library.keySet()) {
            if (library.get(key).getYear() == year) {
                foundedBooks.add(library.get(key));
            }
        }
        return foundedBooks;
    }

    public Optional<Book> getByISBN(String isbn) {
            return Optional.ofNullable(library.get(isbn));
    }

//    public String getISBNFromTitle(String title) {
//        for (String isbn : library.keySet()) {
//            if (library.get(isbn).getTitle().equalsIgnoreCase(title)) {
//                return isbn;
//            }
//        }
//        return null;
//    }

    public void loadDBFromFile() {
        for (String line : getAllLines()) {
            String[] lineItems = line.split(";");
            Book book = new Book(lineItems[0], lineItems[1], lineItems[2], Integer.parseInt(lineItems[3]), lineItems[4]);
            addBook(book);
        }
    }

    public void saveDBToFile(BookLibraryStateDB bookLibraryStateDB) {
        try {
            FileWriter fw = new FileWriter("library.txt", false);
            for (String isbn : library.keySet()) {
                StringBuilder s = new StringBuilder();
                Book book = library.get(isbn);
                s.append(book.getTitle() + ";");
                s.append(book.getAuthorName() + ";");
                s.append(book.getAuthorSurname() + ";");
                s.append(book.getYear() + ";");
                s.append(book.getISBN() + ";");
                s.append(bookLibraryStateDB.getQuantity(isbn) + ";");
                s.append(bookLibraryStateDB.getLendQuantity(isbn) + "\n");
                fw.write(s.toString());
            }
            fw.close();
        } catch (IOException e) {
            System.out.println("Błąd pliku! " + e);
        }
    }

    private List<String> getAllLines() {
        try {
            String filename = "library.txt";
            return Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

package com.programator.zadanie02_booklibrary;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class BookLibraryStateDB {
    private Map<String, Integer> allBooks = new HashMap<>();
    private Map<String, Integer> lendBooks = new HashMap<>();


    public void loadDBFromFile() {
        for (String line : getAllLines()) {
            String[] lineItems = line.split(";");
            allBooks.put(lineItems[4], Integer.parseInt(lineItems[5]));
            lendBooks.put(lineItems[4], Integer.parseInt(lineItems[6]));
        }
    }

    public void addBook(String isbn) {
        allBooks.put(isbn, 1);
        lendBooks.put(isbn, 0);
    }

    public Set<String> getISBNsOfAllAvailableBooks() {
        Set<String> foundedISBNs = new HashSet<>();
        for (String isbn : allBooks.keySet()) {
            if (getQuantity(isbn) - getLendQuantity(isbn) > 0) {
                foundedISBNs.add(isbn);
            }
        }
        return foundedISBNs;
    }

    public boolean isBookAvailable(String isbn) {
        return getQuantity(isbn) - getLendQuantity(isbn) > 0;
    }

    public int getQuantity(String isbn) {
        return allBooks.get(isbn);
    }

    public int getLendQuantity(String isbn) {
        return lendBooks.get(isbn);
    }

    public int getAvailableQuantity(String isbn) {
        return getQuantity(isbn) - getLendQuantity(isbn);
    }

    public void setLendQuantity(String isbn) {
        lendBooks.put(isbn, getLendQuantity(isbn) + 1);
    }

    private List<String> getAllLines() {
        try {
            String filename = "library.txt";
            return Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

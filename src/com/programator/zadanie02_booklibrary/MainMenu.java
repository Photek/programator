package com.programator.zadanie02_booklibrary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainMenu {

    public String showMainMenu() throws IOException {
        System.out.println("1. Wyświetl listę wszystkich książek.");
        System.out.println("2. Wyszukaj książkę.");
        System.out.println("3. Podaruj książkę.");
        System.out.println("4. Wyjdź.");
        System.out.println("Wybierz 1-4: ");

        while (true) {
            String input = readInput();
            switch (input.toLowerCase()) {
                case "1":
                    return "1";
                case "2":
                    return "2";
                case "3":
                    return "3";
                case "4":
                    System.out.println("Bye, bye...");
                    return "exit";
                case "exit":
                    System.out.println("Bye, bye...");
                    return "exit";
                default:
                    System.out.println("Błąd! Wybierz poprawne polecenie (1-4): ");
            }
        }
    }

    public String showSubMenu1() throws IOException {
        System.out.println();
        System.out.println("1. Wyświetl tylko dostępne książki.");
        System.out.println("2. Wypożycz książkę.");
        System.out.println("3. Powrót do menu głównego.");
        System.out.println("Wybierz 1-3: ");

        while (true) {
            String input = readInput();
            switch (input.toLowerCase()) {
                case "1":
                    return "11";
                case "2":
                    return "12";
                case "3":
                    return "mm";
                case "exit":
                    System.out.println("Bye, bye...");
                    return "exit";
                default:
                    System.out.println("Błąd! Wybierz poprawne polecenie (1-3): ");
            }
        }
    }

    public String showSubMenu11() throws IOException {
        System.out.println("1. Wyświetl wszystkie książki.");
        System.out.println("2. Wypożycz książkę.");
        System.out.println("3. Powrót do menu głównego.");
        System.out.println("Wybierz 1-3: ");

        while (true) {
            String input = readInput();
            switch (input.toLowerCase()) {
                case "1":
                    return "1";
                case "2":
                    return "12";
                case "3":
                    return "mm";
                case "exit":
                    System.out.println("Bye, bye...");
                    return "exit";
                default:
                    System.out.println("Błąd! Wybierz poprawne polecenie (1-3): ");
            }
        }
    }

    public String showSubMenu12() throws IOException {
        System.out.println("Wpisz tytuł książki, którą chcesz wypożyczyć:");
        String input = readInput();
        return input;
    }

    public String showSubMenu2() throws IOException {
        System.out.println("1. Wyszukaj po tytule.");
        System.out.println("2. Wyszukaj po nazwisku autora.");
        System.out.println("3. Wyszukaj po dacie wydania.");
        System.out.println("4. Wyszukaj po numerze ISBN.");
        System.out.println("5. Powrót do menu głównego.");
        System.out.println("Wybierz 1-5: ");

        while (true) {
            String input = readInput();
            switch (input.toLowerCase()) {
                case "1":
                    return "21";
                case "2":
                    return "22";
                case "3":
                    return "23";
                case "4":
                    return "24";
                case "5":
                    return "mm";
                case "exit":
                    System.out.println("Bye, bye...");
                    return "exit";
                default:
                    System.out.println("Błąd! Wybierz poprawne polecenie (1-5): ");
            }
        }
    }

    public String showSubMenu21() throws IOException {
        System.out.println("Wpisz poszukiwany tytuł:");
        String input = readInput();
        return input;
    }

    public String showSubMenu22() throws IOException {
        System.out.println("Wpisz nazwisko autora:");
        String input = readInput();
        return input;
    }

    public int showSubMenu23() throws IOException {
        Integer result = -1;
        System.out.println("Wpisz datę wydania:");
        String input = readInput();
        try {
            result = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            System.out.println("Błąd! Podaj poprawny rok.");
        }
        return result;
    }

    public String showSubMenu24() throws IOException {
        System.out.println("Wpisz numer ISBN:");
        String input = readInput();
        return input;
    }

    public String showSubMenu3() throws IOException {
        System.out.println("Chcę podarować książkę o nr ISBN: ");
        String input = readInput();
        return input;
    }

    public String title() throws IOException {
        System.out.println("Podaj tytuł: ");
        String input = readInput();
        return input;
    }

    public String name() throws IOException {
        System.out.println("Podaj imię autora: ");
        String input = readInput();
        return input;
    }

    public String surname() throws IOException {
        System.out.println("Podaj nazwisko autora: ");
        String input = readInput();
        return input;
    }

    public int year() throws IOException {
        int result = -1;
        System.out.println("Podaj rok wydania: ");
        String input = readInput();
        try {
            result = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            System.out.println("Błąd! Podaj poprawny rok.");
            year();
        }
        return result;
    }

    public static String readInput() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }

}

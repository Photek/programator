package com.programator.zadanie03_rentavehicle;

public class Car extends Vehicle {
    private final int hourPrice = 20;
    private final int dayPrice = 120;

    @Override
    public int getHourPrice() {
        return hourPrice;
    }

    @Override
    public int getDayPrice() {
        return dayPrice;
    }

    public Car(int id, String brand, String model, int year, String engineType, double engineSize, String regNo) {
        super(id, brand, model, year, engineType, engineSize, regNo);
    }

    @Override
    public char getRequiredCat() {
        return 'B';
    }

}

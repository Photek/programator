package com.programator.zadanie03_rentavehicle;

public class Motocycle extends Vehicle {
    private final int hourPrice = 15;
    private final int dayPrice = 100;

    @Override
    public int getHourPrice() {
        return hourPrice;
    }

    @Override
    public int getDayPrice() {
        return dayPrice;
    }

    public Motocycle(int id, String brand, String model, int year, String engineType, double engineSize, String regNo) {
        super(id, brand, model, year, engineType, engineSize, regNo);
    }

    @Override
    public char getRequiredCat() {
        return 'A';
    }


}

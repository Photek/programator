package com.programator.zadanie03_rentavehicle;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RentAVehicle {
    public static void main(String[] args) throws IOException {
        UsersDB usersDB = new UsersDB();
        usersDB.loadUsersDBFromFile();

        User loggedUser = null;

        VehiclesDB vehiclesDB = new VehiclesDB();
        vehiclesDB.loadUsersDBFromFile();

        ReservationsDB reservationsDB = new ReservationsDB();
        reservationsDB.loadReservationsDBFromFile(vehiclesDB);


        RentInterface rI = new RentInterface();
        String menuItem = rI.showMainMenu();

        while (!(menuItem.equalsIgnoreCase("exit"))) {
            switch (menuItem) {
                case "1":       // zaloguj sie
                    String login = rI.showLoginDialog();
                    if (usersDB.isUserInDB(login)) {
                        int loginAttempts = 2;

                        while (loginAttempts > 0) {
                            String pass = rI.showPassDialog(loginAttempts);
                            pass = String.valueOf(pass.hashCode());
                            if (usersDB.isPassValid(login, pass)) {
                                loggedUser = usersDB.getUserByLogin(login);
                                System.out.println("Witaj " + login + " !");
                                if (reservationsDB.isUserInReservationsDB(loggedUser)) {
                                    System.out.println("Masz już zarezerwowany pojazd.");
                                    System.out.println();
                                }
                                menuItem = rI.showSubMenu1();
                                break;
                            } else {
                                loginAttempts -= 1;
                                if (loginAttempts == 0) {
                                    System.out.println("Złe hasło! Wykorzystałeś wszystkie próby :(");
                                    menuItem = rI.showMainMenu();
                                } else {
                                    System.out.println("Złe hasło! Spróbuj jeszcze raz.");
                                }
                            }
                        }

                    } else {
                        System.out.println("Podanego loginu nie ma w bazie! Spróbuj jeszcze raz.");
                        menuItem = rI.showMainMenu();
                    }
                    break;
                case "1a":
                    menuItem = rI.showSubMenu1();
                    break;
                case "2":       // zarejestruj sie
                    login = rI.showLoginDialog();
                    if (usersDB.isUserInDB(login)) {
                        System.out.println("Użytkownik o podanym loginie jest już w bazie.");
                        menuItem = rI.showMainMenu();
                    } else {
                        boolean catA = rI.showCatDialog('A');
                        boolean catB = rI.showCatDialog('B');
                        boolean catC = rI.showCatDialog('C');
                        String pass = rI.showPassDialog();
                        pass = String.valueOf(pass.hashCode());
                        User user = new User(login, pass, catA, catB, catC);
                        usersDB.addUser(user);
                        usersDB.saveUserDBToFile();
                        loggedUser = user;
                        System.out.println("Witaj " + login + " !");
                        menuItem = rI.showSubMenu1();
                    }
                    break;
                case "3":       // wyjdz
                    menuItem = "exit";
                    System.out.println("Bye, bye...");
                    break;
                case "11":      // wyswietl liste pojazdow
                    List<Vehicle> listOfVehicles = new ArrayList<>(vehiclesDB.getListOfVehicles());
                    listOfVehicles.sort(new VehicleComparator());
                    for (Vehicle vehicle : listOfVehicles) {
                        System.out.println(vehicle);
                    }
                    System.out.println();
                    if (reservationsDB.isUserInReservationsDB(loggedUser)) {
                        menuItem = rI.showSubMenu1();
                    } else {
                        menuItem = rI.showSubMenu11();
                    }
                    break;
                case "12":      // wyswietl rezerwacje
                    reservationsDB.getReservations();
                    if (reservationsDB.isUserInReservationsDB(loggedUser)) {
                        menuItem = rI.showSubMenu11a();
                    } else {
                        menuItem = rI.showSubMenu1();
                    }
                    break;
                case "13":      // wyloguj
                    loggedUser = null;
                    System.out.println("WYLOGOWANIE");
                    menuItem = rI.showMainMenu();
                    break;
                case "111":     // wypozycz
                    boolean flaga = true;
                    int ID = rI.getIDofVehicleToRentDialog();
                    Optional<Vehicle> vehicleOp = vehiclesDB.getVehicleByID(ID);
                    if (vehicleOp.isPresent()) {
                        Vehicle vehicle = vehicleOp.get();

                        if (loggedUser.isUserAuthorized(vehicle.getRequiredCat())) {
                            reservationsDB.getVehicleReservations(vehicle);

                            while (flaga) {
                                LocalDate startRentDate = rI.showRentStartDateDialog();
                                LocalTime startRentTime = rI.showRentStartTimeDialog();
                                LocalDate endRentDate = rI.showRentEndDateDialog(startRentDate);
                                LocalTime endRentTime = rI.showRentEndTimeDialog(startRentTime);
                                LocalDateTime startRent = LocalDateTime.of(startRentDate, startRentTime);
                                LocalDateTime endRent = LocalDateTime.of(endRentDate, endRentTime);

                                if (reservationsDB.isRentTimeAvailable(vehicle, startRent, endRent)) {
                                    Reservation reservation = new Reservation(loggedUser.getLogin(), vehicle, startRent, endRent);
                                    reservationsDB.addReservation(reservation);
                                    reservationsDB.saveReservationsDBToFile();
                                    System.out.println("Pojazd zarezerwowany.");
                                    reservationsDB.getRentPrice(vehicle, startRent, endRent);
                                    System.out.println();
                                    flaga = false;
                                } else {
                                    System.out.println("Podany termin nie jest dostępny. Spróbuj jeszcze raz.");
                                    reservationsDB.getVehicleReservations(vehicle);
                                    System.out.println();
                                }

                            }

                        } else {
                            System.out.println("Nie masz uprawnień do kierowania tym pojazdem.");
                            System.out.println("Wymagana kategoria prawa jazdy: " + vehicle.getRequiredCat());
                            System.out.println();
                        }
                    } else {
                        System.out.println("Podałeś zły nr. Spróbuj jeszcze raz.");
                        System.out.println();
                        menuItem = rI.showSubMenu11();
                        break;
                    }
                    menuItem = rI.showSubMenu1();
                    break;
                case "111a":
                    reservationsDB.removeReservation(loggedUser);
                    reservationsDB.saveReservationsDBToFile();
                    System.out.println("Rezerwacja usunięta!");
                    System.out.println();
                    menuItem = rI.showSubMenu1();
                    break;
            }
        }
    }
}

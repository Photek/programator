package com.programator.zadanie03_rentavehicle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class RentInterface {

    public String showMainMenu() throws IOException {
        System.out.println("1. Zaloguj się.");
        System.out.println("2. Zarejestruj się.");
        System.out.println("3. Wyjdź.");
        while (true) {
            String input = readInput();
            switch (input.toLowerCase()) {
                case "1":
                    return "1";
                case "2":
                    return "2";
                case "3":
                    return "3";
                case "exit":
                    return "3";
                default:
                    System.out.println("Błąd! Wybierz poprawne polecenie (1-3): ");
            }
        }
    }

    public String showSubMenu1() throws IOException {
        System.out.println("1. Wyświetl listę pojazdów.");
        System.out.println("2. Pokaż rezerwacje.");
        System.out.println("3. Wyloguj.");
        System.out.println("4. Wyjdź.");
        while (true) {
            String input = readInput();
            switch (input.toLowerCase()) {
                case "1":
                    return "11";
                case "2":
                    return "12";
                case "3":
                    return "13";
                case "4":
                    return "3";
                case "exit":
                    return "3";
                default:
                    System.out.println("Błąd! Wybierz poprawne polecenie (1-4): ");
            }
        }
    }

    public String showSubMenu11() throws IOException {
        System.out.println("1. Wypożycz pojazd.");
        System.out.println("2. Wyloguj.");
        System.out.println("3. Wróć menu wyżej.");
        System.out.println("4. Wyjdź.");
        while (true) {
            String input = readInput();
            switch (input.toLowerCase()) {
                case "1":
                    return "111";
                case "2":
                    return "13";
                case "3":
                    return "1a";
                case "4":
                    return "3";
                case "exit":
                    return "3";
                default:
                    System.out.println("Błąd! Wybierz poprawne polecenie (1-4): ");
            }
        }
    }

    public String showSubMenu11a() throws IOException {
        System.out.println("1. Kasuj rezerwacje.");
        System.out.println("2. Wyloguj.");
        System.out.println("3. Wróć menu wyżej.");
        System.out.println("4. Wyjdź.");
        while (true) {
            String input = readInput();
            switch (input.toLowerCase()) {
                case "1":
                    return "111a";
                case "2":
                    return "13";
                case "3":
                    return "1a";
                case "4":
                    return "3";
                case "exit":
                    return "3";
                default:
                    System.out.println("Błąd! Wybierz poprawne polecenie (1-4): ");
            }
        }
    }

    public String showLoginDialog() throws IOException {
        System.out.println("Podaj login: ");
        String input = readInput();
        return input;
    }

    public String showPassDialog(int proby) throws IOException {
        System.out.println("Podaj hasło (zostało prób: " + proby + "): ");
        String input = readInput();
        return input;
    }

    public String showPassDialog() throws IOException {
        System.out.println("Podaj hasło: ");
        String input = readInput();
        return input;
    }

    public boolean showCatDialog(char category) throws IOException {
        boolean flag = true;
        boolean result = false;
        System.out.println("Czy posiadasz prawo jazdy kategorii '" + category + "'? (t/n): ");
        while (flag) {
            String input = readInput();
            switch (input.toLowerCase()) {
                case "t":
                    result = true;
                    flag = false;
                    break;
                case "n":
                    flag = false;
                    break;
                default:
                    System.out.println("Błąd! Odpowiedz 't' = tak lub 'n' = nie.");
            }
        }
        return result;
    }

    public int getIDofVehicleToRentDialog() throws IOException {
        System.out.println("Podaj nr ID pojazdu, który chcesz wypożyczyć:");
        String input = readInput();
        return Integer.parseInt(input);
    }

    public LocalDate showRentStartDateDialog() throws IOException {
        boolean flaga = true;
        LocalDate result = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        System.out.println("Podaj datę rozpoczęcia wynajmu (dd-mm-yyyy):");
        while (flaga) {
            String input = readInput();
            try {
                result = LocalDate.parse(input, formatter);
                flaga = false;
            } catch (Exception e) {
                System.out.println("Zły format daty. Spróbuj jeszcze raz.");
                System.out.println("Podaj datę rozpoczęcia wynajmu (dd-mm-yyyy):");
            }
        }
        return result;
    }

    public LocalTime showRentStartTimeDialog() throws IOException {
        boolean flaga = true;
        LocalTime result = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        System.out.println("Podaj godzinę rozpoczęcia wynajmu (hh:mm):");
        while (flaga) {
            String input = readInput();
            try {
                result = LocalTime.parse(input, formatter);
                flaga = false;
            } catch (Exception e) {
                System.out.println("Zły format godziny. Spróbuj jeszcze raz.");
                System.out.println("Podaj godzinę rozpoczęcia wynajmu (hh:mm):");
            }
        }
        return result;
    }

    public LocalDate showRentEndDateDialog(LocalDate startRentDate) throws IOException {
        boolean flaga = true;
        LocalDate result = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        System.out.println("Podaj datę zakończenia wynajmu (dd-mm-yyyy):");
        while (flaga) {
            String input = readInput();
            try {
                result = LocalDate.parse(input, formatter);
                if (result.isBefore(startRentDate)) {
                    System.out.println("Data zakończenia wynajmu nie może być wcześniejsza niż data rozpoczęcia wynajmu.");
                    System.out.println("Spróbuj jeszcze raz.");
                    System.out.println("Podaj datę zakończenia wynajmu (dd-mm-yyyy):");
                } else {
                    flaga = false;
                }
            } catch (Exception e) {
                System.out.println("Zły format daty. Spróbuj jeszcze raz.");
                System.out.println("Podaj datę zakończenia wynajmu (dd-mm-yyyy):");
            }
        }
        return result;
    }

    public LocalTime showRentEndTimeDialog(LocalTime startRentTime) throws IOException {
        boolean flaga = true;
        LocalTime result = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        System.out.println("Podaj godzinę zakończenia wynajmu (hh:mm):");
        while (flaga) {
            String input = readInput();
            try {
                result = LocalTime.parse(input, formatter);
                if (result.isBefore(startRentTime)) {
                    System.out.println("Czas zakończenia wynajmu nie może być wcześniejszy niż czas rozpoczęcia wynajmu.");
                    System.out.println("Spróbuj jeszcze raz.");
                    System.out.println("Podaj godzinę zakończenia wynajmu (hh:mm):");
                } else {
                    flaga = false;
                }
            } catch (Exception e) {
                System.out.println("Zły format godziny. Spróbuj jeszcze raz.");
                System.out.println("Podaj godzinę zakończenia wynajmu (hh:mm):");
            }
        }
        return result;
    }

    public static String readInput() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }
}

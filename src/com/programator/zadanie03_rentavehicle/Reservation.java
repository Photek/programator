package com.programator.zadanie03_rentavehicle;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Reservation {
    private String userLogin;
    private Vehicle vehicle;
    private LocalDateTime startRent;
    private LocalDateTime endRent;

    public Reservation(String userLogin, Vehicle vehicle, LocalDateTime startRent, LocalDateTime endRent) {
        this.userLogin = userLogin;
        this.vehicle = vehicle;
        this.startRent = startRent;
        this.endRent = endRent;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public LocalDateTime getStartRent() {
        return startRent;
    }

    public LocalDateTime getEndRent() {
        return endRent;
    }

    @Override
    public String toString() {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
        String startRentDate = startRent.toLocalDate().format(dateFormatter);
        String startRentTime = startRent.toLocalTime().format(timeFormatter);
        String endRentDate = endRent.toLocalDate().format(dateFormatter);
        String endRentTime = endRent.toLocalTime().format(timeFormatter);

        return "Użytkownik: " + userLogin + " | " + "Pojazd: " + vehicle.getBrand() + " - " + vehicle.getModel() + " | "
                + "Start wynajmu: " + startRentDate + " -> " + startRentTime
                + " | Koniec wynajmu: " + endRentDate + " -> " + endRentTime;
    }
}

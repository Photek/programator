package com.programator.zadanie03_rentavehicle;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReservationsDB {
    Map<String, Reservation> reservationDB = new HashMap<>();

    public void addReservation(Reservation reservation) {
        reservationDB.put(reservation.getUserLogin(), reservation);
    }

    public void removeReservation(User user) {
        reservationDB.remove(user.getLogin());
    }

    public void getReservations() {
        System.out.println("Rezerwacje: ");
        for (String login : reservationDB.keySet()) {
            System.out.println(reservationDB.get(login));
        }
        if (reservationDB.isEmpty()) System.out.println("--- nie ma rezerwacji ---");
        System.out.println();
    }

    public void getVehicleReservations(Vehicle vehicle) {
        boolean flaga = false;
        StringBuilder s = new StringBuilder();

        for (String key : reservationDB.keySet()) {
            if (reservationDB.get(key).getVehicle().equals(vehicle)) {
                s.append(reservationDB.get(key).toString() + "\n");
                flaga = true;
            }
        }

        if (flaga) {
            System.out.println("Pojazd jest już zarezerwowany w podanych terminach:");
            System.out.println(s.toString());
        }

    }

    public boolean isRentTimeAvailable(Vehicle vehicle, LocalDateTime startRent, LocalDateTime endRent) {
        for (String key : reservationDB.keySet()) {
            Reservation reservation = reservationDB.get(key);
            if (reservation.getVehicle().equals(vehicle)) {
                if ((startRent.isBefore(reservation.getEndRent())) & (startRent.isAfter(reservation.getStartRent()))) {
                    return false;
                }
                if ((endRent.isBefore(reservation.getEndRent())) & (endRent.isAfter(reservation.getStartRent()))) {
                    return false;
                }
            }
        }
        return true;
    }

    public void getRentPrice(Vehicle vehicle, LocalDateTime startRent, LocalDateTime endRent) {
        Duration duration = Duration.between(startRent, endRent);
        long rentAllHours = duration.toHours();
        long rentDays = rentAllHours / 24;
        long rentHours = rentAllHours % 24;
        if (rentHours < 1) rentHours = 1;
        long finalPrice = rentDays * vehicle.getDayPrice() + rentHours * vehicle.getHourPrice();
        System.out.println("Cena doby: " + vehicle.getDayPrice() + " zł" + ", cena godziny: " + vehicle.getHourPrice()+" zł");
        System.out.println("Cena wynajmu łącznie za: " + rentDays + " dni i " + rentHours + " godzin wynosi: " + finalPrice + " zł.");
    }

    public boolean isUserInReservationsDB(User user) {
        for (String login : reservationDB.keySet()) {
            if (user.getLogin().equals(login)) return true;
        }
        return false;
    }

    public void loadReservationsDBFromFile(VehiclesDB vehiclesDB) {
        for (String reservationLine : getReservationLines()) {
            String[] resItem = reservationLine.split(";");
            Reservation reservation = new Reservation(resItem[0], vehiclesDB.getVehicleByRegNo(resItem[1]), LocalDateTime.parse(resItem[2]), LocalDateTime.parse(resItem[3]));
            addReservation(reservation);
        }
    }

    public void saveReservationsDBToFile() {
        try {
            FileWriter fw = new FileWriter("reservations.txt", false);
            for (String key : reservationDB.keySet()) {
                StringBuilder s = new StringBuilder();
                Reservation reservation = reservationDB.get(key);
                s.append(reservation.getUserLogin() + ";");
                s.append(reservation.getVehicle().getRegNo() + ";");
                s.append(reservation.getStartRent().toString() + ";");
                s.append(reservation.getEndRent().toString() + "\n");
                fw.write(s.toString());
            }
            fw.close();
        } catch (IOException e) {
            System.out.println("Błąd pliku!");
        }
    }

    private List<String> getReservationLines() {
        try {
            String filename = "reservations.txt";
            return Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

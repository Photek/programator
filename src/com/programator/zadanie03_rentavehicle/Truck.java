package com.programator.zadanie03_rentavehicle;

public class Truck extends Vehicle {
    private final int hourPrice = 40;
    private final int dayPrice = 500;

    @Override
    public int getHourPrice() {
        return hourPrice;
    }

    @Override
    public int getDayPrice() {
        return dayPrice;
    }
    public Truck(int id, String brand, String model, int year, String engineType, double engineSize, String regNo) {
        super(id, brand, model, year, engineType, engineSize, regNo);
    }

    @Override
    public char getRequiredCat() {
        return 'C';
    }

}

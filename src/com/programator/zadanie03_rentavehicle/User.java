package com.programator.zadanie03_rentavehicle;

public class User {
    private String login;
    private String password;
    private boolean catA;
    private boolean catB;
    private boolean catC;

    public User(String login, String password, boolean catA, boolean catB, boolean catC) {
        this.login = login;
        this.password = password;
        this.catA = catA;
        this.catB = catB;
        this.catC = catC;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isCatA() {
        return catA;
    }

    public void setCatA(boolean catA) {
        this.catA = catA;
    }

    public boolean isCatB() {
        return catB;
    }

    public void setCatB(boolean catB) {
        this.catB = catB;
    }

    public boolean isCatC() {
        return catC;
    }

    public void setCatC(boolean catC) {
        this.catC = catC;
    }

    public boolean isUserAuthorized(char drivingLicenseCategory) {
        boolean result = false;
        switch (drivingLicenseCategory) {
            case 'A':
                result = this.isCatA();
                break;
            case 'B':
                result = this.isCatB();
                break;
            case 'C':
                result = this.isCatC();
                break;
        }
        return result;
    }

    @Override
    public String toString() {
        return login + " | " + password + " | " + catA + " | " + catB + " | " + catC;
    }
}

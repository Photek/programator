package com.programator.zadanie03_rentavehicle;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsersDB {
    Map<String, User> usersDB = new HashMap<>();

    public void addUser(User user) {
        usersDB.put(user.getLogin(), user);
    }

    public boolean isUserInDB(String login) {
        return usersDB.containsKey(login);
    }

    public boolean isPassValid(String login, String pass) {
        if (pass.equals(usersDB.get(login).getPassword())) {
            return true;
        }
        return false;
    }

    public User getUserByLogin(String login){
        return usersDB.get(login);
    }

    public void loadUsersDBFromFile() {
        for (String usersLine : getUsersLines()) {
            String[] lItem = usersLine.split(";");
            User user = new User(lItem[0], lItem[1], Boolean.parseBoolean(lItem[2]), Boolean.parseBoolean(lItem[3]), Boolean.parseBoolean(lItem[4]));
            addUser(user);
        }
    }

    public void saveUserDBToFile() {
        try {
            FileWriter fw = new FileWriter("users.txt", false);
            for (String key : usersDB.keySet()) {
                StringBuilder s = new StringBuilder();
                User user = usersDB.get(key);
                s.append(user.getLogin() + ";");
                s.append(user.getPassword() + ";");
                s.append(user.isCatA() + ";");
                s.append(user.isCatB() + ";");
                s.append(user.isCatC() + "\n");
                fw.write(s.toString());
            }
            fw.close();
        } catch (IOException e) {
            System.out.println("Błąd pliku!");
        }
    }

    private List<String> getUsersLines() {
        try {
            String filename = "users.txt";
            return Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


package com.programator.zadanie03_rentavehicle;

import java.util.Objects;

public abstract class Vehicle {
    protected int id;
    protected String brand;
    protected String model;
    protected int year;
    protected String engineType;
    protected double engineSize;
    protected String regNo;

    public abstract int getHourPrice();

    public abstract int getDayPrice();

    public abstract char getRequiredCat();

    public Vehicle(int id, String brand, String model, int year, String engineType, double engineSize, String regNo) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.engineType = engineType;
        this.engineSize = engineSize;
        this.regNo = regNo;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getRegNo() {
        return regNo;
    }

    public int getId() {
        return id;
    }



    @Override
    public String toString() {
        return id + " | " + brand + " | " + model + " | "
                + year + " | " + engineType + " | " + engineSize
                + " | " + regNo + " | " + getRequiredCat();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vehicle vehicle = (Vehicle) o;

        return Objects.equals(regNo, vehicle.regNo);
    }

    @Override
    public int hashCode() {
        return regNo != null ? regNo.hashCode() : 0;
    }
}

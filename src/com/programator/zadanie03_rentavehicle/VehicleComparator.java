package com.programator.zadanie03_rentavehicle;

import java.util.Comparator;

public class VehicleComparator implements Comparator<Vehicle> {

    @Override
    public int compare(Vehicle o1, Vehicle o2) {
        if (o1.getId() == o2.getId()) {
            return 0;
        }
        if (o1.getId() > o2.getId()) {
            return 1;
        }
        return -1;
    }
}

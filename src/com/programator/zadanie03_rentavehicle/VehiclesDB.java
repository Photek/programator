package com.programator.zadanie03_rentavehicle;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class VehiclesDB {
    Map<String, Vehicle> vehiclesDB = new HashMap<>();

    public void addVehicle(Vehicle vehicle) {
        vehiclesDB.put(vehicle.getRegNo(), vehicle);
    }

    public Collection<Vehicle> getListOfVehicles() {
        return vehiclesDB.values();
    }

    public Optional<Vehicle> getVehicleByID(int ID) {
        Vehicle result = null;
        for (String regNo : vehiclesDB.keySet()) {
            if (ID == vehiclesDB.get(regNo).getId()) {
                result = vehiclesDB.get(regNo);
            }
        }
        return Optional.ofNullable(result);
    }

    public Vehicle getVehicleByRegNo(String regNo){
        return vehiclesDB.get(regNo);
    }

    public void loadUsersDBFromFile() {
        for (String usersLine : getVehiclesLines()) {
            String[] lItem = usersLine.split(";");
            int id = Integer.parseInt(lItem[0]);
            String brand = lItem[1];
            String model = lItem[2];
            int year = Integer.parseInt(lItem[3]);
            String engineType = lItem[4];
            double engineSize = Double.parseDouble(lItem[5]);
            String regNo = lItem[6];
            Vehicle vehicle = null;
            switch (lItem[7]) {
                case "A":
                    vehicle = new Motocycle(id, brand, model, year, engineType, engineSize, regNo);
                    break;
                case "B":
                    vehicle = new Car(id, brand, model, year, engineType, engineSize, regNo);
                    break;
                case "C":
                    vehicle = new Truck(id, brand, model, year, engineType, engineSize, regNo);
                    break;

            }
            addVehicle(vehicle);
        }
    }

    private List<String> getVehiclesLines() {
        try {
            String filename = "vehicles.txt";
            return Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
